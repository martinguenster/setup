{
  pkgs,
  config,
  ...
}
: let
  cfg = config.services.forgejo;
  srv = cfg.settings.server;
in {
  services.forgejo = {
    enable = true;

    settings.server = {
      DOMAIN = "pc.fritz.box";
      HTTP_PORT = 3000;
    };
  };

  services.gitea-actions-runner = {
    package = pkgs.forgejo-actions-runner;
    instances.docker = {
      enable = true;
      name = "docker";
      url = "http://pc.fritz.box:3000";
      # Obtaining the path to the runner token file may differ
      token = "fOWTuFqIwoAvSxUcszoI04poFZxklz2PqYqJNX6d";
      labels = ["bookworm:docker://node:bookworm"];
    };

    instances.nix = {
      enable = true;
      name = "nix";
      url = "http://pc.fritz.box:3000";
      # Obtaining the path to the runner token file may differ
      token = "fOWTuFqIwoAvSxUcszoI04poFZxklz2PqYqJNX6d";
      labels = ["nix:host"];
      hostPackages = with pkgs; [
        bash
        coreutils
        curl
        gawk
        gitMinimal
        gnused
        nodejs
        wget
        nix
      ];
    };
  };

  virtualisation.docker.enable = true;
}
