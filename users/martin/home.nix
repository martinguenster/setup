{pkgs, ...}: let
  username = "martin";
in {
  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = ["qemu:///system"];
      uris = ["qemu:///system"];
    };
  };

  gtk = {
    enable = true;
    cursorTheme = {
      name = "Bibata-Modern-Ice";
      package = pkgs.bibata-cursors;
    };
  };

  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    clojure
    djlint
    elixir
    elixir-ls
    fd
    file
    fira-code
    gnome-secrets
    guile
    htop
    iosevka-comfy.comfy
    klavaro
    mermaid-cli
    mg
    mongodb-compass
    nixd
    nodejs_20
    pgformatter
    pyright
    python314
    ripgrep
    rlwrap
    ruff
    sqlite
    sqlite-utils
    sqlitebrowser
    typescript-language-server
    uv
  ];

  programs = {
    firefox.enable = true;

    direnv = {
      enable = true;
      enableBashIntegration = true;
      nix-direnv.enable = true;
    };

    bash = {
      enable = true;
      shellAliases = {sql = "rlwrap sqlite3";};
    };

    git = {
      enable = true;
      userName = "Martin Günster";
      userEmail = "mail@martinguenster.de";
      extraConfig = {
        init.defaultBranch = "main";
        core.editor = "mg";
      };
      aliases = {
        cl = "config --global -l";
        cm = "commit -m";
        co = "checkout";
        d = "diff";
        last = "log -1 HEAD";
        ll = "log --oneline";
        rv = "remote -v";
        s = "stage";
        st = "status -sb";
        u = "reset HEAD --";
        unstage = "reset HEAD --";
      };
    };

    emacs = {
      enable = true;
      package = pkgs.emacs-pgtk;
      extraPackages = epkgs:
        with epkgs; [
          avy
          cape
          consult
          corfu
          eglot
          elfeed
          elixir-mode
          elixir-yasnippets
          embark
          embark-consult
          emmet-mode
          geiser
          geiser-guile
          inf-elixir
          js2-mode
          magit
          marginalia
          markdown-mode
          mermaid-mode
          nix-mode
          orderless
          org
          org-modern
          php-mode
          pyvenv
          rainbow-delimiters
          rg
          sed-mode
          sqlformat
          symbols-outline
          treesit-grammars.with-all-grammars
          verb
          vertico
          vterm
          wgrep
          ws-butler
          yasnippet
          yasnippet-snippets
          #themes
          doom-themes
          ef-themes
          modus-themes
        ];
      extraConfig = builtins.readFile ./init.el;
    };

    vscode = {
      enable = true;
      profiles.default = {
        enableExtensionUpdateCheck = false;
        enableUpdateCheck = false;
        extensions = with pkgs.vscode-extensions; [
          betterthantomorrow.calva
          elixir-lsp.vscode-elixir-ls
          jnoortheen.nix-ide
        ];
        userSettings = {
          workbench.colorTheme = "Tomorrow Night Blue";
          editor = {
            fontFamily = "Fira Code";
            fontLigatures = true;
            formatOnSave = true;
          };
        };
      };
    };
  };

  home = {
    inherit username;
    homeDirectory = "/home/${username}";
    stateVersion = "24.05";
  };
}
