{pkgs, ...}: {
  imports = [./hardware-configuration.nix];

  nixpkgs.config.allowUnfree = true;

  nix = {
    settings = {
      trusted-users = ["martin"];
      experimental-features = ["nix-command" "flakes"];
    };
  };

  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };
    grub = {
      enable = true;
      devices = ["nodev"];
      efiSupport = true;
      useOSProber = true;
    };
  };

  time.timeZone = "Europe/Berlin";

  i18n = {
    defaultLocale = "de_DE.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "de_DE.UTF-8";
      LC_IDENTIFICATION = "de_DE.UTF-8";
      LC_MEASUREMENT = "de_DE.UTF-8";
      LC_MONETARY = "de_DE.UTF-8";
      LC_NAME = "de_DE.UTF-8";
      LC_NUMERIC = "de_DE.UTF-8";
      LC_PAPER = "de_DE.UTF-8";
      LC_TELEPHONE = "de_DE.UTF-8";
      LC_TIME = "de_DE.UTF-8";
    };
  };
  console.keyMap = "de-latin1-nodeadkeys";

  services.xserver = {
    xkb.layout = "de";
    enable = true;
    desktopManager.gnome.enable = true;
    displayManager.gdm.enable = true;
  };

  environment = {
    variables.EDITOR = "mg";
    systemPackages = with pkgs; [mg];
    sessionVariables = rec {XDG_CONFIG_HOME = "$HOME/.config";};
    gnome.excludePackages = with pkgs; [
      atomix
      cheese
      epiphany
      geary
      gnome-characters
      gnome-music
      gnome-tour
      hitori
      iagno
      tali
      totem
    ];
  };

  users.users.martin = {
    isNormalUser = true;
    description = "Martin";
    extraGroups = ["wheel" "networkmanager" "libvirtd"];
  };

  networking = {
    hostName = "pc";
    firewall.enable = false;
    networkmanager.enable = true;
  };

  system.stateVersion = "24.05";

  programs.nh = {
    enable = true;
    clean.enable = true;
    clean.extraArgs = "--keep 3";
    flake = "/home/martin/git/setup";
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
    localNetworkGameTransfers.openFirewall = true; # Open ports in the firewall for Steam Local Network Game Transfers
  };
}
