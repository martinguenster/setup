{pkgs, ...}: {
  imports = [./hardware-configuration.nix];

  nixpkgs.config.allowUnfree = true;

  nix = {
    settings = {
      trusted-users = ["martin"];
      experimental-features = ["nix-command" "flakes"];
    };
  };

  disko.devices.disk.main.device = "/dev/sda";

  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };
    systemd-boot = {
      configurationLimit = 10;
      enable = true;
    };
  };

  time.timeZone = "Europe/Berlin";

  i18n = {
    defaultLocale = "de_DE.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "de_DE.UTF-8";
      LC_IDENTIFICATION = "de_DE.UTF-8";
      LC_MEASUREMENT = "de_DE.UTF-8";
      LC_MONETARY = "de_DE.UTF-8";
      LC_NAME = "de_DE.UTF-8";
      LC_NUMERIC = "de_DE.UTF-8";
      LC_PAPER = "de_DE.UTF-8";
      LC_TELEPHONE = "de_DE.UTF-8";
      LC_TIME = "de_DE.UTF-8";
    };
  };
  console.keyMap = "de-latin1-nodeadkeys";

  environment.sessionVariables = rec {XDG_CONFIG_HOME = "$HOME/.config";};

  services.xserver = {
    xkb.layout = "de";
    enable = true;
    desktopManager.gnome.enable = true;
    displayManager.gdm.enable = true;
  };

  environment = {
    gnome.excludePackages = with pkgs; [
      atomix
      cheese
      epiphany
      geary
      gnome-characters
      gnome-music
      gnome-tour
      hitori
      iagno
      tali
      totem
    ];
  };

  users.users.martin = {
    isNormalUser = true;
    description = "Martin";
    extraGroups = ["wheel" "networkmanager" "libvirtd"];
  };

  networking = {
    hostName = "x220";
    firewall.enable = false;
    networkmanager.enable = true;
  };

  system.stateVersion = "24.05";

  programs.nh = {
    enable = true;
    clean.enable = true;
    clean.extraArgs = "--keep 3";
    flake = "/home/martin/git/setup";
  };
}
